import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JPopupMenu.Separator;

public class Compra extends JFrame implements ActionListener{
	
	private JButton frango, farrinha, arroz, massa, finalizar;
	private JLabel prod, iva, total;
	private JTextArea lista;
	private JScrollPane pane;
	private JMenuBar menus;
	private JMenu menu;
	private JMenuItem programador;
	private JMenuItem sair;
	private Separator sep;
	
	
	private JPanel painel, p1, p2;
	private JLabel aux1, aux2, aux3;
	private double a = 0, b = 0, c = 0, d = 0;
	
	public Compra(){
		
		frango = new JButton("Frango");
		farrinha = new JButton("Farinha");
		arroz = new JButton("Arroz");
		massa = new JButton("Massa");
		finalizar = new JButton("Finalizar a Compra");
		
		lista = new JTextArea(18, 40);
		pane = new JScrollPane(lista);
		
		programador = new JMenuItem("Dados do programador");
		sair = new JMenuItem("Sair");
		
		menu = new JMenu();
		
		painel = new JPanel();
		painel.setBackground(Color.BLUE);
		
		p1 = new JPanel();
		p2 = new JPanel();
		
		aux1 = new JLabel("");
		aux2 = new JLabel("");
		aux3 = new JLabel("");
		
		sep = new Separator(); 
		
		prod = new JLabel("Valor dos Produtos");
		iva = new JLabel("Valor do IVA");
		total = new JLabel("Valor Total");
		
		
		menus = new JMenuBar();
		menu = new JMenu("Menu");
		
		setTitle("Compra");
		setSize(500, 500);
		setLocation(500, 500);
		//setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		setJMenuBar(menus);
		menus.add(menu);
		menu.add(programador);
		menu.add(sep);
		menu.add(sair);
		
		programador.addActionListener(this);
		sair.addActionListener(this);
		
		painel.setLayout(new GridLayout(4, 1, 5, 5));
		painel.add(frango);
		painel.add(farrinha);
		painel.add(arroz);
		painel.add(massa);
		
		p1.setLayout(new FlowLayout());
		
		p1.add(pane);
		
		p1.add(finalizar);
		
		
		p2.setLayout(new GridLayout(6, 1));
		p2.add(prod);
		p2.add(aux1);
		p2.add(iva);
		p2.add(aux2);
		p2.add(total);
		p2.add(aux3);
		
		add(painel, BorderLayout.WEST);
		add(p2, BorderLayout.EAST);
		add(p1, BorderLayout.CENTER);
		
		frango.addActionListener(this);
		farrinha.addActionListener(this);
		massa.addActionListener(this);
		arroz.addActionListener(this);
		lista.setEditable(false);
		finalizar.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent g) {
		if(g.getSource()==frango){
			lista.setText(lista.getText()+"Frango - 120.00MT \n");
			a = 120;
			
		}
		
		if(g.getSource()==arroz){
			lista.setText(lista.getText()+"Arroz - 20.00MT \n");
			b = 20;
		}
		
		if(g.getSource()==farrinha){
			
			lista.setText(lista.getText()+"Farrinha - 80.00MT \n");
			c = 80;
		}
		
		if(g.getSource()==massa){
			
			lista.setText(lista.getText()+"Massa - 35.00MT \n");
			d = 35;
		}
		
		if(g.getSource()==finalizar){
			
			aux1.setText(""+(a+b+c+d));
			aux2.setText(""+(a+b+c+d)*0.17);
			aux3.setText(""+aux1.getText()+aux2.getText());
			
			
		}
		
if(g.getSource()== sair){
			
			setVisible(false);;
		}
		
		if(g.getSource()==programador){
			
			JOptionPane.showMessageDialog(this, "Guidion I23");
			
		}
	}

public static void main(String[] args) {
	new Compra().setVisible(true);
}
	
}