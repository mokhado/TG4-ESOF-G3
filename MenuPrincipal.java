package Teste3;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MenuPrincipal extends JFrame implements ActionListener{
	
	private JLabel l1, l2, l3, l4, l5;
	private JCheckBox comercial;
	private JPanel painel1, painel2, painel3;
	
	public MenuPrincipal(){
		
		l1 = new JLabel("     Commits");
		l2 = new JLabel("I42");
		l3 = new JLabel("ISUTC");
		l4 = new JLabel("2017");
		l5 = new JLabel("Clique abaixo para iniciar o programa");
		comercial = new JCheckBox("Comercial");
		
		painel1 = new JPanel();
		painel2 = new JPanel();
		painel3= new JPanel();
		
		l3.setForeground(Color.RED);
		l1.setForeground(Color.BLUE);
		l2.setForeground(Color.BLUE);
		l4.setForeground(Color.BLUE);
		
		l5.setFont(new Font("Calibri", Font.BOLD, 18));
		
		setTitle("Menu Principal");
		setSize(400, 400);
		setLocation(500, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new GridLayout(3, 1));
		
		
		painel1.setLayout(new GridLayout(1, 3));
		painel1.add(l1);
		painel1.add(l2);
		painel1.add(l3);
		painel1.add(l4);
		
		painel2.setLayout(new FlowLayout(FlowLayout.CENTER));
		painel2.add(l5);
		
		painel3.setLayout(new FlowLayout(FlowLayout.CENTER));
		painel3.add(comercial);
		
		add(painel1);
		add(painel2);
		add(painel3);
		
	
		comercial.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		
		if(a.getSource()==comercial){
			
			new Compra().setVisible(true);
		}
		
	}

	public static void main(String[] args) {
		new MenuPrincipal().setVisible(true);
	}
}
